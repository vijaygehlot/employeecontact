import React from 'react';

export default () => {
  return (
    <div>
      <h1 className="display-4">About Employee Manager</h1>
      <p className="lead">Simple app to manage Employee Details</p>
      <p className="text-secondary">Version 1.0.0</p>
    </div>
  );
};
