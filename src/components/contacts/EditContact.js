import React, { Component } from 'react';
import TextInputGroup from '../layout/TextInputGroup';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getContact, updateContact } from '../../actions/contactActions';

class EditContact extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      phone: "",
      errors: {},
      username: "",
      address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
          lat: "-37.3159",
          lng: "81.1496"
        }
      },
      website: "",
      company: {
        name: "",
        catchPhrase: "",
        bs: ""
      }
    };
  }

  componentWillReceiveProps(nextProps, nextState) {
    const {
      name,
      email,
      phone,
      address,
      username,
      website,
      company
    } = nextProps.contact;
    this.setState({
      name,
      email,
      phone,
      address,
      username,
      website,
      company
    });
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    console.log("id", this.props.match.params);
    this.props.getContact(id);
  }

  onSubmit = e => {
    e.preventDefault();

    const {
      name,
      email,
      phone,
      address,
      username,
      website,
      company
    } = this.state;

    // Check For Errors
    if (name === "") {
      this.setState({ errors: { name: "Name is required" } });
      return;
    }

    if (email === "") {
      this.setState({ errors: { email: "Email is required" } });
      return;
    }

    if (phone === "") {
      this.setState({ errors: { phone: "Phone is required" } });
      return;
    }

    const { id } = this.props.match.params;

    const updContact = {
      id,
      name,
      email,
      phone,
      address,
      username,
      website,
      company
    };

    this.props.updateContact(updContact);
    

    // Clear State
    this.setState({
      name: "",
      email: "",
      phone: "",
      errors: {},
      address: {},
      username,
      website,
      company: {}
    });

    this.props.history.push("/");
  };

  onChange = e => this.setState({ [e.target.name]: e.target.value });
  handleChange = ({ target }) => {
    const { address } = this.state;

    this.setState(preState => ({
      address: {
        ...preState.address,
        [target.name]: target.value
      }
    }));
  };
  handleComChange = ({ target }) => {
    const { company } = this.state;

    this.setState(preState => ({
      company: {
        ...preState.company,
        [target.name]: target.value
      }
    }));
  };

  render() {
    const {
      name,
      email,
      phone,
      errors,
      address,
      username,
      website,
      company
    } = this.state;

    return (
      <div className="card mb-3">
        <div className="card-header">Edit Employee Contact</div>
        <div className="card-body">
          <form onSubmit={this.onSubmit}>
            <h2>Personal Informations:--</h2>
            <TextInputGroup
              label="Name"
              name="name"
              placeholder="Enter Name"
              value={name}
              onChange={this.onChange}
              error={errors.name}
            />
            <TextInputGroup
              label="Email"
              name="email"
              type="email"
              placeholder="Enter Email"
              value={email}
              onChange={this.onChange}
              error={errors.email}
            />
            <TextInputGroup
              label="Phone"
              name="phone"
              placeholder="Enter Phone"
              value={phone}
              onChange={this.onChange}
              error={errors.phone}
            />
            <TextInputGroup
              label="username"
              name="username"
              placeholder="Enter username"
              value={username}
              onChange={this.onChange}
              error={errors.username}
            />

            <h2>Address:--</h2>
            <TextInputGroup
              label="City"
              name="city"
              placeholder="city"
              value={address.city}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="street"
              name="street"
              placeholder="street"
              value={address.street}
              onChange={this.handleChange.bind(this)}
            />
            <TextInputGroup
              label="suite"
              name="suite"
              placeholder="suite"
              value={address.suite}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="zipcode"
              type="number"
              name="zipcode"
              placeholder="zipcode"
              value={address.zipcode}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <h2>Company Details:--</h2>
            <TextInputGroup
              label="website"
              name="website"
              placeholder="website"
              value={website}
              onChange={this.onChange}
            />
            <TextInputGroup
              label="Company Name"
              name="name"
              placeholder="Company Name"
              value={company.name}
              onChange={this.handleComChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="catchPhrase"
              name="catchPhrase"
              placeholder="catchPhrase"
              value={company.catchPhrase}
              onChange={this.handleComChange.bind(this)}
            />
            <TextInputGroup
              label="company bs"
              name="bs"
              placeholder="company bs"
              value={company.bs}
              onChange={this.handleComChange.bind(this)}
              error={errors.username}
            />
            <input
              type="submit"
              value="Update Employee"
              className="btn btn-light btn-block"
            />
          </form>
        </div>
      </div>
    );
  }
}

EditContact.PropTypes = {
  contact: PropTypes.object.isRequired,
  getContact: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  contact: state.contact.contact
});

export default connect(mapStateToProps, { getContact, updateContact })(EditContact);
