import React, { Component } from 'react';
import TextInputGroup from '../layout/TextInputGroup';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addContact } from '../../actions/contactActions';

class AddContact extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      username: "",
      address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
          lat: "-37.3159",
          lng: "81.1496"
        }
      },
      email: "",
      phone: "",
      errors: {},
      website: "",
      company: {
        name: "",
        catchPhrase: "",
        bs: ""
      }
    };
  }

  // handleData = e => {
  //   this.setState({ [e.target.name]: e.target.value });
  // }
  handleStudentChange = e => {
    // this.setState({ [e.target.name]: event.target.value, edit: true});
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChange = ({ target }) => {
    const { address } = this.state;

    this.setState(preState => ({
      address: {
        ...preState.address,
        [target.name]: target.value
      }
    }));
  };
  handleComChange = ({ target }) => {
    const { company } = this.state;

    this.setState(preState => ({
      company: {
        ...preState.company,
        [target.name]: target.value
      }
    }));
  };
  onSubmit = e => {
    e.preventDefault();

    const {
      name,
      email,
      phone,
      username,
      address,
      company,
      website
    } = this.state;

    // Check For Errors
    if (name === "") {
      this.setState({ errors: { name: "Name is required" } });
      return;
    }

    if (email === "") {
      this.setState({ errors: { email: "Email is required" } });
      return;
    }

    if (phone === "") {
      this.setState({ errors: { phone: "Phone is required" } });
      return;
    }

    const newContact = {
      name,
      username,
      email,
      phone,
      address,
      company,
      website
    };

    this.props.addContact(newContact);

    // Clear State
    this.setState({
      name: "",
      email: "",
      phone: "",
      username: "",
      errors: {},
      address: {},
      website: "",
      company: {}
    });
    console.log("newContact", newContact);

    this.props.history.push("/");
  };

  render() {
    const {
      name,
      email,
      phone,
      errors,
      username,
      address,
      website,
      company
    } = this.state;

    return (
      <div className="card mb-3">
        <div className="card-header">Add Employee Contact</div>
        <div className="card-body">
          <form onSubmit={this.onSubmit}>
              <h2>Personal Informations:--</h2>
            <TextInputGroup
              label="Name"
              name="name"
              placeholder="Enter Name"
              value={name}
              onChange={this.handleStudentChange}
              error={errors.name}
            />
            <TextInputGroup
              label="User Name"
              name="username"
              placeholder="User Name"
              value={username}
              onChange={this.handleStudentChange}
              error={errors.username}
            />
            <TextInputGroup
              label="Email"
              name="email"
              type="email"
              placeholder="Enter Email"
              value={email}
              onChange={this.handleStudentChange}
              error={errors.email}
            />
            <TextInputGroup
              label="Phone"
              name="phone"
              type="number"
              placeholder="Enter Phone"
              value={phone}
              onChange={this.handleStudentChange}
              error={errors.phone}
            />
            <h2>Address:--</h2>
            <TextInputGroup
              label="City"
              name="city"
              placeholder="city"
              value={address.city}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="street"
              name="street"
              placeholder="street"
              value={address.street}
              onChange={this.handleChange.bind(this)}
            />
            <TextInputGroup
              label="suite"
              name="suite"
              placeholder="suite"
              value={address.suite}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="zipcode"
              type="number"
              name="zipcode"
              placeholder="zipcode"
              value={address.zipcode}
              onChange={this.handleChange.bind(this)}
              error={errors.username}
            />
            <h2>Company Details:--</h2>
            <TextInputGroup
              label="website"
              name="website"
              placeholder="website"
              value={website}
              onChange={this.handleStudentChange}
            />
            <TextInputGroup
              label="Company Name"
              name="name"
              placeholder="Company Name"
              value={company.name}
              onChange={this.handleComChange.bind(this)}
              error={errors.username}
            />
            <TextInputGroup
              label="catchPhrase"
              name="catchPhrase"
              placeholder="catchPhrase"
              value={company.catchPhrase}
              onChange={this.handleComChange.bind(this)}
            />
            <TextInputGroup
              label="company bs"
              name="bs"
              placeholder="company bs"
              value={company.bs}
              onChange={this.handleComChange.bind(this)}
              error={errors.username}
            />
            <input
              type="submit"
              value="Add Employee"
              className="btn btn-light btn-block"
            />
          </form>
        </div>
      </div>
    );
  }
}


AddContact.PropTypes = {
  addContact: PropTypes.func.isRequired
}

export default connect(null, { addContact })(AddContact);
