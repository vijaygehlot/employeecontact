import React, { Component } from 'react';
import Contact from './Contact';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getContacts } from '../../actions/contactActions';
import App from '../../App.css'

import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';

class Contacts extends Component {

  constructor(){
    super();
    this.state={
      activePage: 2
    }
  }
  componentDidMount() {
    this.props.getContacts();
  }

  render() {
    const { contacts } = this.props;
    const options = {
      page: 2, // which page you want to show as default
      sizePerPageList: [
        {
          text: "5",
          value: 5
        },
        {
          text: "10",
          value: 10
        },
        {
          text: "All",
          value: contacts.length
        }
      ], // you can change the dropdown list for size per page
      sizePerPage: 5, // which size per page you want to locate as default
      pageStartIndex: 0, // where to start counting the pages
      paginationSize: 3, // the pagination bar size.
      prePage: "Prev", // Previous page button text
      nextPage: "Next", // Next page button text
      firstPage: "First", // First page button text
      lastPage: "Last", // Last page button text
      paginationShowsTotal: this.renderShowsTotal, // Accept bool or function
      paginationPosition: "top" // default is bottom, top and both is all available
      // hideSizePerPage: true > You can hide the dropdown for sizePerPage
      // alwaysShowAllBtns: true // Always show next and previous button
      // withFirstAndLast: false > Hide the going to First and Last page button
    };
    return (
      <React.Fragment>
        <h1 className="display-4 mb-2" style={{ textAlign: "center" }}>
          <span className="text-info">Employees Contact</span> List
        </h1>
        <table class="table">
          <thead>
            <tr style={{ textAlign: "center" }}>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Address</th>
              <th scope="col">Phone</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
        </table>
        {contacts.map(contact => (
          <Contact key={contact.id} contact={contact} />
        ))}

       

        {/* <BootstrapTable
          data={contacts}
          pagination={true}
          options={options}
          pagination
        >
          <TableHeaderColumn dataField="id" isKey>
            Product ID
          </TableHeaderColumn>
          <TableHeaderColumn dataField="name">Product Name</TableHeaderColumn>
          <TableHeaderColumn dataField="price">Product Price</TableHeaderColumn>
        </BootstrapTable> */}
      </React.Fragment>
    );
  }
}

Contacts.propTypes = {
  contacts: PropTypes.array.isRequired,
  getContacts: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  contacts: state.contact.contacts
});

export default connect(mapStateToProps, {getContacts})(Contacts);
